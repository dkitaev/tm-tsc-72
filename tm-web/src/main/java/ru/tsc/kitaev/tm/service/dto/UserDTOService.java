package ru.tsc.kitaev.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.exception.empty.EmptyLoginException;
import ru.tsc.kitaev.tm.repository.dto.UserDTORepository;

@Service
@NoArgsConstructor
public class UserDTOService extends AbstractDTOService<UserDTO> implements IUserDTOService {

    @NotNull
    @Autowired
    private UserDTORepository repository;

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLogin(login);
    }

}
