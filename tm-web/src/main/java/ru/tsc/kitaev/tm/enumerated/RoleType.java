package ru.tsc.kitaev.tm.enumerated;

public enum RoleType {

    ADMINISTRATOR,
    USER;

}
