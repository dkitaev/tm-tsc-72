package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.api.endpoint.TaskEndpoint;
import ru.tsc.kitaev.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.kitaev.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Override
    @NotNull
    @WebMethod
    @PostMapping("/add")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDTO add(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final TaskDTO task
    ) {
        return taskService.add(UserUtil.getUserId(), task);
    }

    @Override
    @NotNull
    @WebMethod
    @PutMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDTO save(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final TaskDTO task
    ) {
        return taskService.update(UserUtil.getUserId(), task);
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public List<TaskDTO> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        return taskService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @Nullable @PathVariable(value = "id") final String id
    ) {
        return taskService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public int getSize() {
        return taskService.getSize(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void clear() {
        taskService.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final TaskDTO task
    ) {
        taskService.delete(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        taskService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAllByProjectId/{projectId}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public List<TaskDTO> findAllByProjectId(
            @WebParam(name = "projectId", partName = "projectId")
            @Nullable @PathVariable(value = "projectId") final String projectId
    ) {
        return taskService.findAllByProjectId(UserUtil.getUserId(), projectId);
    }

}
