package ru.tsc.kitaev.tm.api.service.model;

import ru.tsc.kitaev.tm.model.Project;

public interface IProjectService extends IOwnerService<Project> {

}
