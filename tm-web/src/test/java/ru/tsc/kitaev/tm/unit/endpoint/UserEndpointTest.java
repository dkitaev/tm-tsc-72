package ru.tsc.kitaev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.kitaev.tm.config.DataBaseConfiguration;
import ru.tsc.kitaev.tm.config.WebApplicationConfiguration;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.enumerated.RoleType;
import ru.tsc.kitaev.tm.marker.WebUnitCategory;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
@Category(WebUnitCategory.class)
public class UserEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    private static final String USER_URL = "http://localhost:8080/api/users/";

    @NotNull
    private final UserDTO user1 = new UserDTO();

    private boolean check = true;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("adminTest", "adminTest");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        @NotNull final String passwordHash = passwordEncoder.encode("userTest1");
        user1.setLogin("userTest1");
        user1.setPasswordHash(passwordHash);
        user1.setRole(RoleType.USER);
        add(user1);
    }

    @After
    @SneakyThrows
    public void after() {
        if (check) {
            @NotNull final String url = USER_URL + "deleteById/" + user1.getId();
            mockMvc.perform(MockMvcRequestBuilders.delete(url)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }

    @SneakyThrows
    private void add(@NotNull final UserDTO user) {
        @NotNull final String url = USER_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private UserDTO findById(@NotNull final String id) {
        @NotNull final String url = USER_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, UserDTO.class);
    }

    @Nullable
    @SneakyThrows
    private UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String url = USER_URL + "findByLogin/" + login;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, UserDTO.class);
    }

    @NotNull
    @SneakyThrows
    private List<UserDTO> findAll() {
        @NotNull final String url = USER_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, UserDTO[].class));
    }

    @Test
    @SneakyThrows
    public void saveTest() {
        user1.setLogin("userUpdate");
        @NotNull final String url = USER_URL + "save";
        @NotNull ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user1);
        mockMvc.perform(MockMvcRequestBuilders.put(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final UserDTO user = findById(user1.getId());
        Assert.assertEquals("userUpdate", user.getLogin());
    }

    @Test
    public void findAllTest() {
        @NotNull final List<UserDTO> users = findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(5, users.size());
    }

    @Test
    public void findByIdTest() {
        Assert.assertNotNull(findById(user1.getId()));
    }

    @Test
    public void findByLoginTest() {
        Assert.assertNotNull(findByLogin(user1.getLogin()));
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        @NotNull final String url = USER_URL + "existsById/" + user1.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals("true", objectMapper.readValue(json, String.class));
    }

    @Test
    @SneakyThrows
    public void countTest() {
        @NotNull final String url = USER_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(5, objectMapper.readValue(json, Integer.class).intValue());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = USER_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user1);
        mockMvc.perform(MockMvcRequestBuilders.delete(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(user1.getId()));
        check = false;
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        @NotNull final String url = USER_URL + "deleteById/" + user1.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(user1.getId()));
        check = false;
    }

}
