package ru.tsc.kitaev.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.kitaev.tm.config.DataBaseConfiguration;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.marker.WebUnitCategory;
import ru.tsc.kitaev.tm.service.dto.ProjectDTOService;
import ru.tsc.kitaev.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(WebUnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1", "Test Project Description 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2", "Test Project Description 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3", "Test Project Description 3");

    @NotNull
    @Autowired
    private ProjectDTOService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private String userId;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        projectService.add(userId, project1);
        projectService.add(userId, project2);
        projectService.add(userId, project3);
    }

    @After
    public void after() {
        projectService.clear(userId);
    }

    @Test
    public void addTest() {
        @NotNull final ProjectDTO newProject = new ProjectDTO("Test Project 4", "Test Project Description 4");
        projectService.add(userId, newProject);
        Assert.assertEquals("Test Project 4", newProject.getName());
    }

    @Test
    public void updateTest() {
        project1.setName("Test Project Update");
        projectService.update(userId, project1);
        Assert.assertEquals("Test Project Update", project1.getName());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<ProjectDTO> projects = projectService.findAll(userId);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final ProjectDTO project = projectService.findById(userId, project1.getId());
        Assert.assertEquals(project1.getId(), project.getId());
    }

    @Test
    public void existsByIdTest() {
        Assert.assertFalse(projectService.existsById(userId, ""));
        Assert.assertTrue(projectService.existsById(userId, project1.getId()));
    }

    @Test
    public void getSizeTest() {
        Assert.assertEquals(3, projectService.getSize(userId));
    }

    @Test
    public void clearTest() {
        projectService.clear(userId);
        Assert.assertEquals(0, projectService.getSize(userId));
    }

    @Test
    public void deleteTest() {
        projectService.delete(userId, project3);
        Assert.assertNull(projectService.findById(userId, project3.getId()));
    }

    @Test
    public void deleteByIdTest() {
        projectService.deleteById(userId, project3.getId());
        Assert.assertEquals(2, projectService.getSize(userId));
    }

}
