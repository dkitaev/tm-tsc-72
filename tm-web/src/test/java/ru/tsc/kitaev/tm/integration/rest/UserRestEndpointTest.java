package ru.tsc.kitaev.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;
import ru.tsc.kitaev.tm.config.DataBaseConfiguration;
import ru.tsc.kitaev.tm.config.WebApplicationConfiguration;
import ru.tsc.kitaev.tm.dto.Result;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.enumerated.RoleType;
import ru.tsc.kitaev.tm.marker.WebIntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
@Category(WebIntegrationCategory.class)
public class UserRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/users/";

    @NotNull
    private static final HttpHeaders HEADERS = new HttpHeaders();

    @Nullable
    private static String sessionId;

    @Nullable
    private static String userId;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    private final UserDTO user1 = new UserDTO();

    private boolean check = true;

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=adminTest&password=adminTest";
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADERS.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        @NotNull final String urlProfile = "http://localhost:8080/api/auth/profile";
        @NotNull final ResponseEntity<UserDTO> responseProfile = restTemplate.exchange(urlProfile, HttpMethod.GET, new HttpEntity<>(HEADERS), UserDTO.class);
        userId = responseProfile.getBody().getId();
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<UserDTO> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<UserDTO> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, UserDTO.class);
    }

    @AfterClass
    public static void afterClass() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADERS));
    }

    @Before
    public void before() {
        @NotNull final String url = BASE_URL + "add/";
        @NotNull final String passwordHash = passwordEncoder.encode("userTest1");
        user1.setLogin("userTest1");
        user1.setPasswordHash(passwordHash);
        user1.setRole(RoleType.USER);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(user1, HEADERS));
    }

    @After
    public void after() {
        if (check) {
            @NotNull final String url = BASE_URL + "deleteById/" + user1.getId();
            sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(user1, HEADERS));
        }
    }

    @Test
    public void saveTest() {
        @NotNull final String url = BASE_URL + "save/";
        user1.setLogin("userUpdate");
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(user1, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findById/" + user1.getId();
        Assert.assertEquals("userUpdate", sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().getLogin());
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = BASE_URL + "findAll/";
        Assert.assertEquals(5, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String url = BASE_URL + "findById/" + user1.getId();
        Assert.assertNotNull(sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

    @Test
    public void deleteTest() {
        @NotNull final String url = BASE_URL + "delete/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(user1, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(4, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
        check = false;
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String url = BASE_URL + "deleteById/" + user1.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(user1, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(4, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
        check = false;
    }

}
