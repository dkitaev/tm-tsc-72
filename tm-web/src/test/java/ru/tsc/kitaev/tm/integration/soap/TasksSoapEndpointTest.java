package ru.tsc.kitaev.tm.integration.soap;

import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.kitaev.tm.api.endpoint.AuthEndpoint;
import ru.tsc.kitaev.tm.api.endpoint.ProjectEndpoint;
import ru.tsc.kitaev.tm.api.endpoint.TaskEndpoint;
import ru.tsc.kitaev.tm.client.AuthSoapEndpointClient;
import ru.tsc.kitaev.tm.client.ProjectSoapEndpointClient;
import ru.tsc.kitaev.tm.client.TaskSoapEndpointClient;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.marker.WebIntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(WebIntegrationCategory.class)
public class TasksSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static ProjectEndpoint projectEndpoint;

    @NotNull
    private static TaskEndpoint taskEndpoint;

    @Nullable
    private static String userId;

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1", "Test Project Description 1");

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1", "Test Task Description 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2", "Test Task Description 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3", "Test Task Description 3");

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        userId = authEndpoint.profile().getId();
    }

    @AfterClass
    public static void afterClass() {
        authEndpoint.logout();
    }

    @Before
    public void before() {
        project1.setUserId(userId);
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        task1.setProjectId(project1.getId());
        task2.setProjectId(project1.getId());
        task3.setProjectId(project1.getId());
        projectEndpoint.add(project1);
        taskEndpoint.add(task1);
        taskEndpoint.add(task2);
    }

    @After
    public void after() {
        taskEndpoint.clear();
        projectEndpoint.clear();
    }

    @Test
    public void addTest() {
        taskEndpoint.add(task3);
        @Nullable final TaskDTO task = taskEndpoint.findById(task3.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void saveTest() {
        task1.setName("Test Task Update");
        taskEndpoint.save(task1);
        @Nullable final TaskDTO task = taskEndpoint.findById(task1.getId());
        Assert.assertEquals("Test Task Update", task.getName());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<TaskDTO> tasks = taskEndpoint.findAll();
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final TaskDTO task = taskEndpoint.findById(task1.getId());
        Assert.assertEquals(task1.getId(), task.getId());
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(taskEndpoint.existsById(task1.getId()));
    }

    @Test
    public void getSizeTest() {
        Assert.assertEquals(2, taskEndpoint.getSize());
    }

    @Test
    public void clearTest() {
        taskEndpoint.clear();
        Assert.assertEquals(0, taskEndpoint.getSize());
    }

    @Test
    public void deleteTest() {
        taskEndpoint.delete(task2);
        Assert.assertNull(taskEndpoint.findById(task2.getId()));
    }

    @Test
    public void deleteByIdTest() {
        taskEndpoint.deleteById(task2.getId());
        Assert.assertEquals(1, taskEndpoint.getSize());
    }

    @Test
    public void findAllByProjectIdTest() {
        @Nullable final List<TaskDTO> tasks = taskEndpoint.findAllByProjectId(project1.getId());
        Assert.assertEquals(2, tasks.size());
    }

}
