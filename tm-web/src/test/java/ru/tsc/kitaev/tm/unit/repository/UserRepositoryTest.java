package ru.tsc.kitaev.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.config.DataBaseConfiguration;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.enumerated.RoleType;
import ru.tsc.kitaev.tm.marker.WebUnitCategory;
import ru.tsc.kitaev.tm.repository.dto.UserDTORepository;

import java.util.List;
import java.util.Optional;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(WebUnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private final UserDTO user1 = new UserDTO();

    @NotNull
    @Autowired
    private UserDTORepository userRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private boolean check = true;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("adminTest", "adminTest");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        @NotNull final String passwordHash = passwordEncoder.encode("userTest1");
        user1.setLogin("userTest1");
        user1.setPasswordHash(passwordHash);
        user1.setRole(RoleType.USER);
        userRepository.save(user1);
    }

    @After
    public void after() {
        if (check)
            userRepository.deleteById(user1.getId());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<UserDTO> users = userRepository.findAll();
        Assert.assertEquals(5, users.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Optional<UserDTO> user = userRepository.findById(user1.getId());
        Assert.assertEquals(user1.getId(), user.orElse(null).getId());
    }

    @Test
    public void findByLoginTest() {
        @Nullable final UserDTO user = userRepository.findByLogin(user1.getLogin());
        Assert.assertEquals(user1.getLogin(), user.getLogin());
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(userRepository.existsById(user1.getId()));
    }

    @Test
    public void countTest() {
        Assert.assertEquals(5, userRepository.count());
    }

    @Test
    public void deleteByIdTest() {
        userRepository.deleteById(user1.getId());
        Assert.assertEquals(4, userRepository.count());
        check = false;
    }

}
