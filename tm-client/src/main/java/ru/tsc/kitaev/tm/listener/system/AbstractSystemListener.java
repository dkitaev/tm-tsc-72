package ru.tsc.kitaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kitaev.tm.listener.AbstractListener;

public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

}
