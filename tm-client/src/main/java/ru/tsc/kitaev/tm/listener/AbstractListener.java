package ru.tsc.kitaev.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.ISessionService;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListener {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected ProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @NotNull
    public abstract String command();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void handler(@NotNull final ConsoleEvent consoleEvent);

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    @Override
    public String toString() {
        String result = "";
        @Nullable String name = command();
        @Nullable String description = description();
        if (name != null) result += name + " ";
        if (description != null) result += ":" + description;
        return result;
    }

}
